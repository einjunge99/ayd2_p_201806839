const { Builder, By, until } = require('selenium-webdriver');
const { expect } = require('chai');
const dotenv = require('dotenv');
dotenv.config();

const USERNAME = process.env.LT_USERNAME;
const KEY = process.env.LT_ACCESS_KEY;
const HOST = process.env.PAGE_HOST;

// gridUrl: gridUrl can be found at automation dashboard
const GRID_HOST = 'hub.lambdatest.com/wd/hub';

// Setup Input capabilities
const capabilities = {
    platform: 'Windows 10',
    browserName: 'Chrome',
    version: '92.0',
    resolution: '1024x768',
    network: true,
    visual: true,
    console: true,
    video: true,
    name: 'Test', // name of the test
    build: 'NodeJS build' // name of the build
}

// Dummy data
const post = {
    author: 'Isaac Maldonado',
    content: 'This is a test'
}
//const disclaimer = "It's so quiet here... go ahead and share your thoughts!"

// URL: https://{username}:{accessKey}@hub.lambdatest.com/wd/hub
const gridUrl = 'https://' + USERNAME + ':' + KEY + '@' + GRID_HOST;
describe('webdriver', () => {
    const driver = new Builder()
        .usingServer(gridUrl)
        .withCapabilities(capabilities)
        .build();

    it('should fail submit with empty fields', async () => {
        await driver.get(`${HOST}`);
        await driver.findElement(By.name('selenium-submit')).click();
        await driver.wait(until.alertIsPresent());
        const alert = await driver.switchTo().alert();
        const alertText = await alert.getText();
        await alert.accept();
        expect(alertText).to.equal('Something went wrong...');
    });

    it('should success with filled fields', async () => {
        await driver.findElement(By.name('author')).sendKeys(post.author);
        await driver.findElement(By.name('content')).sendKeys(post.content);
        await driver.findElement(By.name('selenium-submit')).click();
        const alert = await driver.switchTo().alert();
        const alertText = await alert.getText();
        await alert.accept();
        expect(alertText).to.equal('Thought added!');
    });

    it(`should render at least one post created by ${post.author}`, async () => {
        const author = await driver.findElement(By.id(`selenium-${post.author}`)).getText();
        expect(author).to.equal(post.author);
        await driver.close()
    });
})
