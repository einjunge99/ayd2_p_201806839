import React from 'react'

export const Card = ({ content, author, createdAt }) => {
    return (
        <div className="column is-half" >
            <div className="card">
                <div className="card-content">
                    <p className="title has-text-black">
                        {content}
                    </p>
                    <p id={`selenium-${author}`} className="subtitle has-text-black">
                        {author}
                    </p>
                </div>
                <footer className="card-footer">
                    <p className="card-footer-item">
                        <span>
                            Created at: {createdAt}
                        </span>
                    </p>
                    <p className="card-footer-item">
                        <span className="has-text-danger">
                            Remove
                        </span>
                    </p>
                </footer>
            </div>
        </div >
    )
}
