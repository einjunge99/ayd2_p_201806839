import './App.css';
import 'bulma/css/bulma.min.css';
import { Card } from './components/Card';
import { useState } from 'react';

function App() {
  const [cards, setCards] = useState([])
  const [values, setValues] = useState({ author: '', content: '', createdAt: '' })


  const handleChange = (e) => {
    console.log(values)
    setValues({ ...values, [e.target.name]: e.target.value })
  }

  const handelSubmit = (e) => {
    e.preventDefault();
    if (!values.author || !values.content) return alert('Something went wrong...')
    values.createdAt = new Date().toUTCString()
    setCards([...cards, values])
    alert('Thought added!')
  }

  return (
    <>
      <section class="hero is-info is-fullheight">

        <div class="hero-body">
          <div class="container">
            <h1 className="title">Thought it - 201806839</h1>
            <h2 className="subtitle">What are you thinking on?</h2>

            <div className="columns">
              <div className="column">
                <form onSubmit={handelSubmit} className="mb-5">
                  <div className="field">
                    <label className="label has-text-white">Author</label>
                    <div className="control">
                      <input className="input" name="author" type="text" placeholder="Who are you?" onChange={handleChange} />
                    </div>
                  </div>

                  <div className="field">
                    <label className="label has-text-white">Content</label>
                    <div className="control">
                      <textarea className="textarea" name="content" placeholder="Your content" onChange={handleChange}></textarea>
                    </div>
                  </div>

                  <div className="control">
                    <button name="selenium-submit" className="button has-text-weight-boldhas-text-info has-background-white">Share</button>
                  </div>
                </form>

                <div className="columns is-centered">

                  {cards.length === 0 && (
                    <div className="column is-half">
                      <h3 className="title is-3">It's so quiet here... go ahead and share your thoughts!</h3>
                    </div>
                  )}
                  {cards.map(card => (
                    <Card {...card} />
                  ))}
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    </>
  );
}

export default App;
