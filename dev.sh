cp /home/config/front-test/.env ./selenium/.env

sudo docker build -t selenium-client ./selenium --no-cache
sudo docker run --name client-container  selenium-client

sudo docker rm client-container -f 
sudo docker rmi selenium-client -f